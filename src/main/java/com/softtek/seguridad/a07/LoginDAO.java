package com.softtek.seguridad.a07;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class LoginDAO {

	Logger lgr = Logger.getLogger(LoginDAO.class.getName());

	public boolean validate(String name, String pass, Connection con) {
		var status = false;
		PreparedStatement pstmt = null;
		ResultSet rs =null;
		try {
			lgr.log(Level.INFO, "Validate USER");
			pstmt = con.prepareStatement("select * from users where username=? and password=?");
			pstmt.setString(1, name);
			pstmt.setString(2, pass);

			rs = pstmt.executeQuery();
			status = rs.next();

		} catch (SQLException sqlex) {

			lgr.log(Level.SEVERE, sqlex.getMessage(), sqlex);
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (rs != null)
					rs.close();
			} catch (SQLException sqlex) {
				lgr.log(Level.SEVERE, sqlex.getMessage(), sqlex);
			}
		}
		return status;
	}


}
