package com.softtek.seguridad.a07;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginDDL {

	private Connection con;
	Logger lgr = Logger.getLogger(LoginDDL.class.getName());

	LoginDDL(Connection con) {
		this.con = con;

	}

	void createTable() {

		Statement stmt = null;
		String sql;
		try {
			stmt = con.createStatement();

			try {
				lgr.log(Level.INFO, "DROP table USERS");

				sql = "DROP TABLE USERS";
				stmt.executeUpdate(sql);
			} catch (SQLException sqlex) {
				
			}
			lgr.log(Level.INFO, "Creating table USERS");

			sql = "CREATE TABLE   USERS " + "(id INTEGER not NULL, " + " username VARCHAR(255), "
					+ " password VARCHAR(255), " + " PRIMARY KEY ( id ))";
			stmt.executeUpdate(sql);
		} catch (SQLException sqlex) {

			lgr.log(Level.SEVERE, sqlex.getMessage(), sqlex);
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException sqlex) {
				lgr.log(Level.SEVERE, sqlex.getMessage(), sqlex);
			}
		}
	}

	void insertRecords() {
		Statement stmt = null;
		String sql;
		try {
			stmt = con.createStatement();

			lgr.log(Level.INFO, "Insert records");

			sql = "INSERT INTO Users " + "VALUES (1, 'jaas', 'qwerty')";

			stmt.executeUpdate(sql);
			sql = "INSERT INTO Users " + "VALUES (2, 'admin', 'admin')";

			stmt.executeUpdate(sql);
		} catch (SQLException sqlex) {

			lgr.log(Level.SEVERE, sqlex.getMessage(), sqlex);
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException sqlex) {
				lgr.log(Level.SEVERE, sqlex.getMessage(), sqlex);
			}
		}
	}

}
