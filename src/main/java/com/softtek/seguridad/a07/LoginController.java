package com.softtek.seguridad.a07;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginController extends HttpServlet {
	private Connection conn;
	Logger lgr = Logger.getLogger(LoginController.class.getName());

	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		lgr.log(Level.INFO, "INIT ");
		conn = (Connection) getServletContext().getAttribute("connection");
		try {
			conn.setAutoCommit(true);
		} catch (SQLException sqlex) {
			lgr.log(Level.SEVERE, sqlex.getMessage(), sqlex);
		}
		var loginDDL = new LoginDDL(conn);
		loginDDL.createTable();

		loginDDL.insertRecords();

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String username = request.getParameter("username");
		String passw = request.getParameter("userpass");

		var loginDAO = new LoginDAO();


		if (loginDAO.validate(username, passw, conn)) {
			RequestDispatcher rd = request.getRequestDispatcher("welcomeController");
			rd.forward(request, response);
		} else {
			//out.print("Sorry username or password error " + username);
			RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
			rd.include(request, response);
		}

		out.close();
	}

}
