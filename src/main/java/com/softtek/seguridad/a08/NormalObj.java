package com.softtek.seguridad.a08;

import java.io.IOException;
import java.io.Serializable;

class NormalObj implements Serializable {
	public String name;

	public NormalObj(String name) {
		this.name = name;
	}

	private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		System.out.println(this.name);
	}
}