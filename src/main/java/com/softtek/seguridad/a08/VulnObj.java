package com.softtek.seguridad.a08;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;

class VulnObj implements Serializable{
    public String cmd;
    public VulnObj(String cmd){
    this.cmd = cmd;
    }
    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException{
        in.defaultReadObject();
    String s = null;
        Process p = Runtime.getRuntime().exec(this.cmd);
        BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
        while ((s = stdInput.readLine()) != null) {
            System.out.println(s);
        }
    }
}

/*import java.io.Serializable;

public class VulnObj implements Serializable {

	public String cmd;

	public VulnObj(String cmd) {
		this.cmd = cmd;
	}
}
*/
