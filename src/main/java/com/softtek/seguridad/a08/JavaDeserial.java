package com.softtek.seguridad.a08;

import java.io.FileInputStream;
import java.io.ObjectInputStream;


/**
 * Lee el archivo normalObj.serial e intenta realizar 
 * la tranformación al objeto NormalObj
 * 
 * 
 * @author javier.aguilar
 *
 */
public class JavaDeserial {
	
	public static void main(String args[]) throws Exception {

		FileInputStream fis = new FileInputStream("tmpa08/normalObj.serial");
		ObjectInputStream ois = new ObjectInputStream(fis);

		NormalObj unserObj = (NormalObj) ois.readObject();
		ois.close();
	}
}
