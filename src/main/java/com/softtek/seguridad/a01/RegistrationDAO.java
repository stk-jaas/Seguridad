package com.softtek.seguridad.a01;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RegistrationDAO {

	Logger lgr = Logger.getLogger(RegistrationDAO.class.getName());

	public void getRecord(String test, Connection con) {
		Statement stmt = null;
		ResultSet rs = null;
		String result;
		try {

			var sql = "SELECT id, first, last, age FROM Registration where first='" + test + "'";
			lgr.log(Level.INFO, "SQL: " + sql);
			// var sql = "SELECT id, first, last, age FROM Registration where first='Zara'
			// or '1'='1'";
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				// Retrieve by column name
				// Display values

				result = "ID: " + rs.getInt("id") + " Age: " + rs.getInt("age") + " First: " + rs.getString("first")
						+ " Last: " + rs.getString("last");

				lgr.log(Level.INFO, result);
			}

		} catch (SQLException sqlex) {

			lgr.log(Level.SEVERE, sqlex.getMessage(), sqlex);
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
			} catch (SQLException sqlex) {
				lgr.log(Level.SEVERE, sqlex.getMessage(), sqlex);
			}
		}
	}
}
