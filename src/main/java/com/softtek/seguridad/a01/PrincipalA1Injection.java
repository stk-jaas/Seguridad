package com.softtek.seguridad.a01;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PrincipalA1Injection {

	public static void main(String[] args) {

		var url = "jdbc:h2:mem:";

		//"N' or '1'='1"
		var param=args[0];
		
		try (var con = DriverManager.getConnection(url);) {

			var registrationDDL = new RegistrationDDL(con);
			registrationDDL.createTable();

			registrationDDL.insertRecords();
			var registrationDAO =new RegistrationDAO();
			registrationDAO.getRecord(param, con);

		} catch (SQLException ex) {

			var lgr = Logger.getLogger(PrincipalA1Injection.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);
		}
	}
}
