package com.softtek.seguridad.a10;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.softtek.seguridad.a07.LoginController;

public class Test {

	
	static Logger lgr = Logger.getLogger(LoginController.class.getName());

	public static void main(String[] args) {
		lgr.setLevel(Level.FINEST);
		ConsoleHandler handler = new ConsoleHandler();
        // PUBLISH this level
        handler.setLevel(Level.FINEST);
        lgr.addHandler(handler);
        
		lgr.finest("j");
		lgr.info("m");
	}

}
