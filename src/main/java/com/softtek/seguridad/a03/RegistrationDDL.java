package com.softtek.seguridad.a03;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RegistrationDDL {

	private Connection con;
	Logger lgr = Logger.getLogger(RegistrationDDL.class.getName());

	RegistrationDDL(Connection con) {
		this.con = con;

	}

	void createTable() {

		Statement stmt = null;
		try {
			stmt = con.createStatement();

			lgr.log(Level.INFO, "Creating table REGISTRATION");

			String sql = "CREATE TABLE   REGISTRATION " + "(id INTEGER not NULL, " + " first VARCHAR(255), "
					+ " last VARCHAR(255), " + " age INTEGER, " + " PRIMARY KEY ( id ))";
			stmt.executeUpdate(sql);
		} catch (SQLException sqlex) {

			lgr.log(Level.SEVERE, sqlex.getMessage(), sqlex);
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException sqlex) {
				lgr.log(Level.SEVERE, sqlex.getMessage(), sqlex);
			}
		}
	}

	
	void insertRecords() {
		Statement stmt = null;
		String sql;
		try {
			stmt = con.createStatement();

			lgr.log(Level.INFO, "Insert records");

			sql = "INSERT INTO Registration " + "VALUES (100, 'Zara', 'Ali', 18)";

			stmt.executeUpdate(sql);
			sql = "INSERT INTO Registration " + "VALUES (101, 'Mahnaz', 'Fatma', 25)";

			stmt.executeUpdate(sql);
			sql = "INSERT INTO Registration " + "VALUES (102, 'Zaid', 'Khan', 30)";

			stmt.executeUpdate(sql);
			sql = "INSERT INTO Registration " + "VALUES(103, 'Sumit', 'Mittal', 28)";

			stmt.executeUpdate(sql);
		} catch (SQLException sqlex) {

			lgr.log(Level.SEVERE, sqlex.getMessage(), sqlex);
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException sqlex) {
				lgr.log(Level.SEVERE, sqlex.getMessage(), sqlex);
			}
		}
	}

}
