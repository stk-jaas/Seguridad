package com.softtek.seguridad.a03;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RegistrationDAO {

	Logger lgr = Logger.getLogger(RegistrationDAO.class.getName());

	public void getRecord(String test, Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String result;
		try {

			var sql = "SELECT id, first, last, age FROM Registratio where first = ? ";
			lgr.log(Level.INFO, "SQL: " + sql);
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, test);
			rs = pstmt.executeQuery();
			System.out.println("C");
			while (rs.next()) {
				// Retrieve by column name
				// Display values

				result = "ID: " + rs.getInt("id") + " Age: " + rs.getInt("age") + " First: " + rs.getString("first")
						+ " Last: " + rs.getString("last");

				lgr.log(Level.INFO, result);
			}
			System.out.println("D");

		} catch (SQLException sqlex) {
			sqlex.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (rs != null)
					rs.close();
			} catch (SQLException sqlex) {
				sqlex.printStackTrace();
			}
		}
	}
}
