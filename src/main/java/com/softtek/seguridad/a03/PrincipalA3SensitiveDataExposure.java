package com.softtek.seguridad.a03;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PrincipalA3SensitiveDataExposure {
	public static void main(String[] args) {

		var url = "jdbc:h2:mem:";

		var param=args[0];
		
		try (var con = DriverManager.getConnection(url);) {

			var registrationDDL = new RegistrationDDL(con);
			registrationDDL.createTable();
			registrationDDL.insertRecords();
			
			var registrationDAO =new RegistrationDAO();
			registrationDAO.getRecord(param, con);

		} catch (SQLException ex) {

			var lgr = Logger.getLogger(PrincipalA3SensitiveDataExposure.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);
		}
	}
}
