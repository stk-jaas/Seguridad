<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Softtek Web Application</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="container">
		<div class="row text-center login-page">
			<div class="col-md-12 login-form">
				<form action="loginController" method="post">
					<div class="row">
						<div class="col-md-12 login-form-header">
							<p class="login-form-font-header">
								App<span>Softtek</span>
							<p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 login-from-row">
							<input name="username" type="text" placeholder="Username"
								required />
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 login-from-row">
							<input name="userpass" type="password" placeholder="Password"
								required />
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 login-from-row">
							<button class="btn btn-info">Login</button>
						</div>
					</div>
				</form>
			</div>

			<div>
				<%
					String username = request.getParameter("username");
					if (username != null && username.length() > 0) {
				%>
				Error el username :<%=request.getParameter("username")%>
				no existe o el password es incorrecto.
				<%
					}
				%>

			</div>
		</div>

	</div>
</body>
</HTML>